import glob
import os

from setuptools import find_packages
from setuptools import setup

package_name = 'dh_gripper'
share_dir = 'share/' + package_name

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
     data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        (share_dir, ['package.xml']),
        (share_dir + '/launch', glob.glob(os.path.join('launch', '*.launch.py'))),
        (share_dir + '/param', glob.glob(os.path.join('param', '*.yaml'))),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='daegyu',
    maintainer_email='daegyu@telelian.com',
    description='dh gripper package',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'sub = dh_gripper.dh_gripper_driver:main'
        ],
    },
)
