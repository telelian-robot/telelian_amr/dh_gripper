import time
import logging

import serial

from .check_crc import crc16

class AG95Serial:
    def __init__(self, port, baudrate, gripper_id, timeout, log_level):
        logging.basicConfig(format='%(asctime)s [%(levelname)s]:%(message)s', level=log_level)
        logging.info(f"{port}, {baudrate}, {gripper_id}, {timeout}, {log_level}")
        self.gripper_id = gripper_id
        self.open(port=port, baudrate=baudrate, timeout=timeout)
        
    def open(self,port, baudrate, timeout):
        #baudrate 115200, 8 bits, no parity, 1 stop bit 
        self.__ser = serial.Serial(port=port, baudrate=baudrate, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=timeout)
        self.__ser.flush()
        logging.warn(f'AG95 serial {self.__ser.name} opened')

    def close(self):
        self.__ser.close()

    def write(self, index, data):
        send_buf = bytearray([
            self.gripper_id,
            0x06,
            (index >> 8) & 0xff,
            index & 0xff,
            (data >> 8) & 0xff,
            data & 0xff
        ])
        crc = crc16(send_buf, 6)
        send_buf.append(crc & 0xff)
        send_buf.append((crc >> 8) & 0xff)

        self.__ser.write(send_buf)
        rev_buf = self.__ser.read(8)

        for i in range(len(rev_buf)):
            if rev_buf[i] != send_buf[i]:
                logging.error(f"write echo error : {i}")
                return False
        
        return True

    def read(self, index):
        send_buf = bytearray([
            self.gripper_id,
            0x03,
            (index >> 8) & 0xff,
            index & 0xff,
            0x00,
            0x01,
        ])
        crc = crc16(send_buf, 6)
        send_buf.append(crc & 0xff)
        send_buf.append((crc >> 8) & 0xff)
        
        if not self.__ser.write(send_buf):
            logging.error("write error")
            return None

        time.sleep(0.1)
        
        rev_buf = self.__ser.read(8)
        if len(rev_buf) == 7:
            crc = crc16(rev_buf, len(rev_buf) -2)  
            revcrch = rev_buf[6]
            revcrcl = rev_buf[5]
            revcrc = revcrch * 256 + revcrcl
            if crc != revcrc:
                logging.error("crc error")
                return None
            else:
                return ( rev_buf[4] & 0xff | rev_buf[3] << 8 ) & 0xffff
        else:
            logging.error(f"read error {len(rev_buf)} ")
            return None
    