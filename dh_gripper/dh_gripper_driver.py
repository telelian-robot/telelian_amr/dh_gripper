import time

import rclpy
from rclpy.node import Node
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup

from dh_gripper_msg.msg import GripperCtrl
from dh_gripper_msg.srv import GripperState
from .dh_modebus_gripper import DH_Modbus_Gripper


class GripperDriver(Node):
    def __init__(self):
        super().__init__('dh_gripper_subscriber')

        self.declare_parameters(
            namespace='',
            parameters=[
            ('port', '/dev/DH_hand'),
            ('baudrate', 115200),
            ('gripper_id', 0x01),
            ('timeout', 1),
            ('log_level', 'INFO')
            ])
        self._port = self.get_parameter('port').value
        self._baudrate = self.get_parameter('baudrate').value
        self._gripper_id = self.get_parameter('gripper_id').value
        self._timeout = self.get_parameter('timeout').value
        self._log_level = self.get_parameter('log_level').value

        self.get_logger().info(f"port : {self._port}")
        self.get_logger().info(f"baudrate : {self._baudrate}")
        self.get_logger().info(f"gripper_id : {self._gripper_id}")
        self.get_logger().info(f"timeout : {self._timeout}")
        self.get_logger().info(f"log_level : {self._log_level}")


        self.dh_modbus_gripper = DH_Modbus_Gripper(
            port=self._port, 
            baudrate=self._baudrate, 
            gripper_id=self._gripper_id, 
            timeout=self._timeout, 
            log_level=self._log_level)
        
        self.subscription = self.create_subscription(
            GripperCtrl, 
            '/gripper/ctrl', 
            self.update_gripper_control,
            10)

        
        self.callback_group = MutuallyExclusiveCallbackGroup()

        self.get_state_server = self.create_service(
            GripperState,
            '/gripper/get_state',
            self.get_state,
            callback_group=self.callback_group
        )
    
    def get_state(self, request, response):
        if not (0 <= request.type <= 2):
            self.get_logger().error(f"request.type range error : {request.type}")
            response.result = -1
            return response
            
        ret = self.dh_modbus_gripper.get_state(request.type)
        response.result = ret
        return response        

    def update_gripper_control(self, msg):
        if msg.initialize == 1:
            self.dh_modbus_gripper.init_gripper()
        else :
            if 20 <= msg.force <= 100 :
                force = msg.force
            else :
                force = 20
                self.get_logger().error(f"force range erorr : {msg.force} , set to 20%")

            self.dh_modbus_gripper.set_force(force) 

            #self.dh_modbus_gripper.set_speed(msg.speed)

            if 0 <= msg.position <= 1000:
                self.dh_modbus_gripper.set_position(msg.position)
            else :
                self.get_logger().error(f"position range error : {msg.position}")

    def close(self):
        self.dh_modbus_gripper.close()


def main(args=None):
    rclpy.init(args=args)
    
    node = GripperDriver()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.get_logger().info('Keyboard Interrupt (SIGINT)')
    finally:
        node.close()
        node.destroy_node()

if __name__ == '__main__':
    main()


