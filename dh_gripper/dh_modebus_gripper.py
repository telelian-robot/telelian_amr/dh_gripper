import time
import logging
from .dh_gripper_serial import AG95Serial

class DH_Modbus_Gripper():

    def __init__(self, port='/dev/ttyUSB0', baudrate=115200, gripper_id=0x01, timeout=3, log_level='INFO'):
        logging.basicConfig(format='%(asctime)s [%(levelname)s]:%(message)s', level=log_level)

        self._port = port
        self._baudrate = baudrate
        self._gripper_id = gripper_id
        self._timeout = timeout
        self._log_level = log_level

        self.AG95Serial = self.open()
        # intialize gripper
        self.init_gripper()

    def open(self):
        return AG95Serial(port=self._port
                        , baudrate=self._baudrate
                        , gripper_id=self._gripper_id
                        , timeout=self._timeout
                        , log_level=self._log_level)
    
    def init_gripper(self):
        initstat = self.AG95Serial.read(0x0200)
        while initstat is None:
            logging.info("gripper reopen")
            self.close()
            self.AG95Serial = self.open()
            initstat = self.AG95Serial.read(0x0200)
            time.sleep(1)

        if initstat != 1:
            logging.info("send grip init")
            # self.AG95Serial.write(0x0100, 0xA5) # fully initialization
            self.AG95Serial.write(0x0100, 0x01)
            
            logging.info("wait grip initialized")
            time_out = 30
            time.sleep(1)

            while initstat != 1 and time_out > 0:
                initstat = self.AG95Serial.read(0x0200)
                logging.info(f"initstat = {initstat}")
                time.sleep(0.1)
                time_out -= 1

            if initstat != 1:
                logging.info("gripper initialize error")
            else:
                logging.info("gripper initialize success")
        else:
            logging.info("gripper already initialized")

    def set_position(self, position):
        if position >= 0 and position <= 1000:
            self.AG95Serial.write(0x0103, position)
        else:
            logging.info("position range error")

    def get_position(self):
        return self.AG95Serial.read(0x0103)

    def set_force(self, force):
        if force >= 20 and force <= 100:
            self.AG95Serial.write(0x0101, force)
        else:  
            logging.info("force range error")
    
    def get_force(self):
        return self.AG95Serial.read(0x0101)
    
    def set_speed(self, speed):
        if speed >= 0 and speed <= 100:
            self.AG95Serial.write(0x0104, speed)
        else:
            logging.info("speed range error")
    
    def get_speed(self):
        return self.AG95Serial.read(0x0104)
    
    def get_gripper_state(self):
        # 0: in motion
        # 1: Reach position
        # 2: Object caught
        # 3: Object dropped
        return self.AG95Serial.read(0x0201)

    def get_state(self, req_type):
        # req_type
        # 0: initialization state
        # 1: gripper state
        # 2: gripper position
        if 0 <= req_type <= 2:
            return self.AG95Serial.read(0x0200 + req_type)
        else:
            return None

    def close(self):
        self.AG95Serial.close()
        

def main(args=None):
    dh_modbus_gripper = DH_Modbus_Gripper()

    logging.info(f"cur force = {dh_modbus_gripper.get_force()}")
    dh_modbus_gripper.set_force(20)
    dh_modbus_gripper.set_position(1000)
    dh_modbus_gripper.set_position(0)

    logging.info(f"cur force = {dh_modbus_gripper.get_force()}")
    dh_modbus_gripper.set_force(100)
    dh_modbus_gripper.set_position(1000)
    dh_modbus_gripper.set_position(0)

    
    logging.info(f"cur speed = {dh_modbus_gripper.get_speed()}")
    dh_modbus_gripper.set_speed(20)
    
    dh_modbus_gripper.set_position(0)
    dh_modbus_gripper.set_position(1000)

    logging.info(f"cur speed = {dh_modbus_gripper.get_speed()}")
    dh_modbus_gripper.set_speed(100)
    
    dh_modbus_gripper.set_position(0)
    dh_modbus_gripper.set_position(1000)

if __name__ == '__main__':
    main()


